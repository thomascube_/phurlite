<?php

// Adapted version with PHP 5.3 + PDO + SQLite3

define('DBNAME', 'phurlite.sqlite3');
define('SHORT_STR_LEN', 4);
define('SHORT_CHARS', '0123456789abcdefghijklmnopqrstvwxyzABCDEFGHIJKLMNOPQRSTVWXYZ'); 
define('HOST', $_SERVER['HTTP_HOST']);

$create_table = !file_exists(DBNAME);

$db = new SQLite3(DBNAME, SQLITE3_OPEN_READWRITE | SQLITE3_OPEN_CREATE);
if (!$db->lastErrorCode()) {
	if ($create_table) {
		$query = '
			CREATE TABLE "links" (
			"url" TEXT NOT NULL ON CONFLICT IGNORE UNIQUE ON CONFLICT IGNORE,
			"short" TEXT UNIQUE,
			"hits" INTEGER DEFAULT 0
			);
		';
		$db->exec($query);
	}
}
else {
	die($db->lastErrorMsg());
}

if ($_GET['u']) {
	$short_str = add_url($_GET['u'], $db);
	header('Content-type: text/plain');
	echo shortlink($short_str);
	exit();
}

if ($_GET['p']) {
	if ($url = get_url($_GET['p'], $db)) {
		add_hit($url, $db);
		header("Location: $url");
	}
	else {
		header("HTTP/1.0 404 Not Found");
	}
	exit();
}

if (isset($_GET['list'])) {
	$query = "SELECT * FROM links ORDER BY rowid DESC;";
	$res = $db->query($query);
	echo '<table><tr><th>Shortened</th><th>Original</th><th>Hits</th></tr>';
	while ($row = $res->fetchArray()) {
		echo '<tr><td>'.shortlink_html($row['short'])."</td><td>$row[url]</td><td>$row[hits]</td></tr>";
	}
	echo '</table>';
}

function add_url($url, $db) {
	# verify it is unique
	$query = "SELECT short FROM links WHERE url='$url';";
	if ($row = $db->querySingle($query, true)) {
		return $row['short'];
	}
	else {
		$short = get_short_str($url, $db);
		$query = "INSERT INTO links (url, short) VALUES ('$url', '$short');";
		$db->exec($query);
		return $short;
	}
}

function get_short_str($url, $db) {
	$short = substr(str_shuffle(SHORT_CHARS), 0, SHORT_STR_LEN);
	# make sure it is not being used
	$query = "SELECT short FROM links WHERE short='$short';";
	$res = $db->querySingle($query);
	if (!empty($res)) {
		return get_short_str($url, $db);
	}
	return $short;
}

function get_url($short_str, $db) {
	$query = "SELECT url FROM links WHERE short='$short_str';";
	$row = $db->querySingle($query, true);
	return $row['url'];
}

function add_hit($url, $db) {
	$query = "UPDATE links SET hits = hits + 1 WHERE url='$url';";
	$db->exec($query);
}

function shortlink($short_str) {
	return 'http://'.HOST.'/'.$short_str;
}

function shortlink_html($short_str) {
	$shortlink = shortlink($short_str);
	return "<a href=\"$shortlink\">$short_str</a>";
}
